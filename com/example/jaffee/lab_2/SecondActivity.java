package com.example.jaffee.lab_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity implements View.OnClickListener {

    private Button addBookmarkBtn = null;
    private TextView tagTextView = null;
    private TextView urlTextView = null;
    private Integer priority = 0;
    private RadioButton radio1;
    private RadioButton radio2;
    private RadioButton radio3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        tagTextView = findViewById(R.id.editTextTag);
        urlTextView = findViewById(R.id.editTextUrl);
        addBookmarkBtn = findViewById(R.id.buttonAddBookmark);
        addBookmarkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent data = new Intent();
                data.putExtra("Tag", tagTextView.getText().toString());
                data.putExtra("Url", urlTextView.getText().toString());
                data.putExtra("Priority", priority.toString());
                setResult(RESULT_OK, data);
                finish();
            }
        });
        radio1 = findViewById(R.id.radioButtonP1);
        radio2 = findViewById(R.id.radioButtonP2);
        radio3 = findViewById(R.id.radioButtonP3);
        radio1.setOnClickListener(this);
        radio2.setOnClickListener(this);
        radio3.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.radioButtonP1: {
                priority = 1;
            }
            case R.id.radioButtonP2: {
                priority = 2;
            }
            case R.id.radioButtonP3: {
                priority = 3;
            }
        }
    }
}
