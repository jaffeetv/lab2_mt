package com.example.jaffee.lab_2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import static com.example.jaffee.lab_2.Constants.*;

/**
 * Created by jaffee on 1/8/18.
 */

public class SQLiteApp extends SQLiteOpenHelper {

    SQLiteDatabase dealsDB;

    public SQLiteApp(Context context) {
        super(context, YVA_DATABASE_NAME, null  , DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + YVA_TABLE_BOOKMARKS + " (" +
                YVA_BOOKMARKS_ID + " integer primary key autoincrement, " +
                YVA_BOOKMARKS_TAG + " text, " +
                YVA_BOOKMARKS_URL + " text, " +
                YVA_BOOKMARKS_PRIORITY + " text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if((oldVersion == 1) && (newVersion == 2)) {
            db.execSQL("create table " + YVA_TABLE_BOOKMARKS + " (" +
                    YVA_BOOKMARKS_ID + " integer primary key autoincrement, " +
                    YVA_BOOKMARKS_TAG + " text, " +
                    YVA_BOOKMARKS_URL + " text, " +
                    YVA_BOOKMARKS_PRIORITY + " text)"
            );
        }
    }

    public void addDataToDB(BookMark bookmark) {
        dealsDB = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(YVA_BOOKMARKS_TAG, bookmark.Tag);
        values.put(YVA_BOOKMARKS_URL, bookmark.Url);
        values.put(YVA_BOOKMARKS_PRIORITY, bookmark.Priority);
        if(dealsDB.insert(YVA_TABLE_BOOKMARKS, null, values) == -1) {
            Log.e("ERROR", "Can't write to DB");
        }
        dealsDB.close();
    }

    public void getValidDataFromDB(ArrayList<BookMark> bookmarks) {
        dealsDB = getWritableDatabase();
        String[] columns = new String[] {
                YVA_BOOKMARKS_TAG,
                YVA_BOOKMARKS_URL,
                YVA_BOOKMARKS_PRIORITY
        };
        Cursor cursor = dealsDB.query(YVA_TABLE_BOOKMARKS, columns, null, null, null, null, null);
        bookmarks.clear();

        if(!cursor.isAfterLast()) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                BookMark bm = new BookMark();
                bm.Tag = cursor.getString(cursor.getColumnIndex(YVA_BOOKMARKS_TAG));
                bm.Url = cursor.getString(cursor.getColumnIndex(YVA_BOOKMARKS_URL));
                bm.Priority = cursor.getString(cursor.getColumnIndex(YVA_BOOKMARKS_PRIORITY));

                bookmarks.add(bm);
                cursor.moveToNext();
            }
        }

        cursor.close();
        dealsDB.close();
    }
}
