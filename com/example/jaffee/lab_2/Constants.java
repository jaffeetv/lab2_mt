package com.example.jaffee.lab_2;

/**
 * Created by jaffee on 11/20/17.
 */

public class Constants {
    static final String YVA_DATABASE_NAME = "bookmarks.db";
    static final int DATABASE_VERSION = 1;
    static final String YVA_TABLE_BOOKMARKS = "bookmarks";
    static final String YVA_BOOKMARKS_ID = "Id";
    static final String YVA_BOOKMARKS_TAG = "Tag";
    static final String YVA_BOOKMARKS_URL = "URL";
    static final String YVA_BOOKMARKS_PRIORITY = "Priority";
}
