package com.example.jaffee.lab_2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by jaffee on 11/19/17.
 */

public class BookMarksListAdapter extends ArrayAdapter<BookMark> {
    private ArrayList<BookMark> listOfBookMarks = null;

    public BookMarksListAdapter(Context context, int resource, ArrayList<BookMark> objects) {
        super(context, resource, objects);
        listOfBookMarks = objects;
    }

    @Override
    public int getCount() {return listOfBookMarks.size();}

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, null);
        }

        TextView textTag = convertView.findViewById(R.id.textViewTag);
        TextView textUrl = convertView.findViewById(R.id.textViewUrl);
        TextView textPriority = convertView.findViewById(R.id.textViewPriority);
        BookMark bookMark = listOfBookMarks.get(position);
        textTag.setText(bookMark.Tag);
        textUrl.setText(bookMark.Url);
        textPriority.setText(bookMark.Priority);

        return convertView;
    }
}
