package com.example.jaffee.lab_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button startSecActButton = null;
    private ArrayList<BookMark> bookMarks;
    private ListView listView;
    private BookMarksListAdapter adapter;
    private SQLiteApp dbOpenHelper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        startSecActButton = findViewById(R.id.buttonStartSecAct);
        startSecActButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
                startActivityForResult(intent, 1);
            }
        });
        dbOpenHelper = new SQLiteApp(getApplicationContext());
        bookMarks = new ArrayList<>();
        listView = findViewById(R.id.ListView);
        adapter = new BookMarksListAdapter(getApplicationContext(), R.layout.list_item, bookMarks);
        listView.setAdapter(adapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        BookMark bookMark = new BookMark();

        bookMark.Tag = data.getStringExtra("Tag");
        bookMark.Url = data.getStringExtra("Url");
        bookMark.Priority = data.getStringExtra("Priority");

        dbOpenHelper.addDataToDB(bookMark);
        bookMarks.add(bookMark);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        dbOpenHelper.getValidDataFromDB(bookMarks);
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbOpenHelper.close();
    }
}



